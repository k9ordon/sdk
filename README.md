# danube.ai SDK - Examples

This repository shows examples on how to use the services of [danube.ai](https://danube.ai/). Visit the [danube-sdk npm package](https://www.npmjs.com/package/danube-sdk) page to get more information on the interfaces this SDK provides.


## Subscription

Visit https://danube.ai/ if you want to get an API key.


### danube.ai prediction

Under `src/danubePredictionDemo.js` you can find an example on how to use the danube.ai prediction API with this SDK.

You can run the example via:

```shell
$ npm install
$ export API_KEY={my_api_key}
[ $ export DANUBE_API_URL={api_url} ]
$ node src/danubePredictionDemo.js
```

Make sure to replace {my_api_key} with your danube.ai API key.

Optionally you can also specify an api url by replacing {api_url} and including the 3rd line. This defaults to `https://api.danube.ai/`, if not provided.


### danube.ai recommendation

Under `src/danubeRecommendationDemo.js` you can find an example on how to use the danube.ai recommendation API with this SDK.

You can run the example via:

```shell
$ npm install
$ export API_KEY={my_api_key}
[ $ export DANUBE_API_URL={api_url} ]
$ node src/danubeRecommendationDemo.js
```

Make sure to replace {my_api_key} with your danube.ai API key.

Optionally you can also specify an api url by replacing {api_url} and including the 3rd line. This defaults to `https://api.danube.ai/`, if not provided.
