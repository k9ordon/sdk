
const { DanubeClient } = require('danube-sdk');

const danubeClient = new DanubeClient(
  process.env.API_KEY, // specify your api key in the environment (or directly set it here)
  process.env.DANUBE_API_URL, // will fallback to 'https://api.danube.ai/' if null --> won't need to specify this yourself
);

async function simulateSessionStep(session, step, data) {
  console.log('');
  console.log('-------------------- danube recommendation --------------------');
  console.log(`                ----- Session ${session} - Step ${step} -----`);
  console.log('');

  console.log('current session data:');
  console.log(data);
  console.log('');

  // save your rule-set
  const result = await danubeClient.danubeRecommendation(
    JSON.stringify(data),
    3,
  );

  console.log('recommendations:');
  console.log(result);
  console.log('\n');
}

async function runDemo() {
  console.log('');
  console.log('#######################################################');
  console.log('####### starting danube.ai recommendation demo ########');
  console.log('');
  console.log('Note: Running this demo multiple times will yield different results as the first time, as session data has already been recorded then!');
  console.log('');
  console.log(`Created danube.ai client for API key '${process.env.API_KEY}'.`);
  console.log('');

  await simulateSessionStep(1, 1, [{ page: 'Home' }]);
  await simulateSessionStep(1, 2, [{ page: 'Home' }, { page: 'About' }]);
  await simulateSessionStep(1, 3, [{ page: 'Home' }, { page: 'About' }, { page: 'Pricing' }]);
  await simulateSessionStep(2, 1, [{ page: 'Home' }]);
  await simulateSessionStep(2, 2, [{ page: 'Home' }, { page: 'Pricing' }]);
  await simulateSessionStep(2, 2, [{ page: 'Home' }, { page: 'Pricing' }, { page: 'Imprint' }]);
  await simulateSessionStep(3, 1, [{ page: 'Home' }]);

  console.log('####### danube.ai recommendation demo finished ########');
  console.log('#######################################################');
  console.log('');
}

runDemo();
